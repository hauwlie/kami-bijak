<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sidebar-warp columns">
    <div class="row">
        <div class="advertisement">
            <div class="row mt-40">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                    <a href="https://merahputih.com"><img src="<?php echo asset('images/ad/2.png')?>" alt=""></a>
                </div>
            </div>
        </div>
                                   
        <aside class="zm-post-lay-e-area col-sm-6 col-md-12 col-lg-12 mt-70">
            <div class="row mb-40">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="section-title">
                        <h2 class="h6 header-color inline-block uppercase"><i class="fa fa-street-view" aria-hidden="true"></i> Most Viewed</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="zm-posts">
                    @foreach($postsmost as $row)
                        <article class="zm-post-lay-e zm-single-post clearfix">
                            <div class="zm-post-thumb f-left">
                                 <a href="{{ url('detail') }}/{{$row['id']}}"><img src="<?php echo asset('images/post/e/5.jpg')?>" alt="img"></a>
                            </div>
                            <div class="zm-post-dis f-right">
                                <div class="zm-post-header"> 
                                    <?php 
                                        if ($row->category=="Infosiana") {
                                            echo '<div class="zm-category" ><a href="'.url("cat").'/'.$row->category.'" class="bg-cat-1 cat-btn">'.$row->category.'</a></div>';
                                        }elseif ($row->category=="Hiburan") {
                                            echo '<div class="zm-category" ><a href="'.url("cat").'/'.$row->category.'" class="bg-cat-2 cat-btn">'.$row->category.'</a></div>';
                                        } elseif ($row->category=="Kuliner") {
                                            echo '<div class="zm-category" ><a href="'.url("cat").'/'.$row->category.'" class="bg-cat-3 cat-btn">'.$row->category.'</a></div>';
                                        }                  
                                    ?>  
                                    <h2 class="zm-post-title"><a href="{{ url('detail') }}/{{$row['id']}}">{{$row['title']}}</a></h2>
                                </div>
                            </div>
                        </article>
                    @endforeach
                                                                              
                    </div>
                </div>
            </div>
        </aside>

        <div class="advertisement">
            <div class="row mt-40">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="padding-top: 50px; ">
                    <a href="https://merahputih.com"><img src="<?php echo asset('images/ad/2.png')?>" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>