<!-- Start trending post area -->
<div class="trending-post-area xs-pt-35 sm-pt-35">
    <div class="container">
        <div class="trend-post-list zm-posts owl-active-2 clearfix">
        <!-- Start single trend post -->
        <?php $count = 0; ?>
        @foreach ($posts as $item)
        <?php if($count == 6) break; ?>
            <article class="zm-trending-post zm-lay-a zm-single-post" data-dark-overlay="5"  data-scrim-bottom="9" data-effict-zoom="3" style="padding: 10px;  ">
                <div class="zm-post-thumb">
                    <iframe class="slide2" src="{{$item['embed']}}" frameborder="0" allow="encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="zm-post-dis text-white">
                    <div class="zm-category">
                        <?php 
                            if ($item->category=="Infosiana") {
                                echo '<div class="zm-category" ><a href="'.url("cat").'/'.$item->category.'" class="bg-cat-1 cat-btn">'.$item->category.'</a></div>';
                            }elseif ($item->category=="Hiburan") {
                                 echo '<div class="zm-category" ><a href="'.url("cat").'/'.$item->category.'" class="bg-cat-2 cat-btn">'.$item->category.'</a></div>';
                            } elseif ($item->category=="Kuliner") {
                                echo '<div class="zm-category" ><a href="'.url("cat").'/'.$item->category.'" class="bg-cat-3 cat-btn">'.$item->category.'</a></div>';
                            }                  
                        ?>  
                    </div>
                    <p class="zm-post-title"><a href="{{ url('detail') }}/{{$item['id']}}">{{$item['title']}}</a></p>
                                <!-- <div class="zm-post-meta">
                                    <ul>
                                        <li class="s-meta"><a href="#" class="zm-author">Thomson Smith</a></li>
                                        <li class="s-meta"><a href="#" class="zm-date">April 18, 2016</a></li>
                                    </ul>
                                </div> -->
                </div>
            </article>
        <?php $count++; ?>
        @endforeach
                    <!-- End single trend post -->
                     
                   
        </div>
    </div>
</div>
<!-- End trending post area -->