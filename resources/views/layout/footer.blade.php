<footer id="footer" class="footer-wrapper footer-2">
    <div class="footer-top-wrap ptb-70 bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="zm-widget pr-20">
                        <h2 class="h6 zm-widget-title uppercase text-white mb-30">About </h2>
                        <div class="zm-widget-content">
                            <p>KamiBijak adalah sebuah media video online yang ramah disabilitas dalam bentuk visual Bahasa Isyarat dan Teks bagi penyandang disabilitas khusus Tuli.</p>
                            <div class="zm-social-media zm-social-2">
                                <ul>
                                    <li><a href="https://www.facebook.com/KamiBijakID"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://twitter.com/KamiBijakID"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="https://www.instagram.com/kamibijakid"><i class="fa fa-instagram"></i></a></li>                                    
                                    <li><a href="https://www.youtube.com/channel/UClH-iALhZ2niASkp-XSNGpA"><i class="fa fa-youtube"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="zm-widget pl-20 pr-40">
                        <h2 class="h6 zm-widget-title uppercase text-white mb-30">Social Links</h2>
                        <div class="zm-widget-content">
                            <div class="zm-category-widget zm-category-1">
                                @foreach($postsre as $row)
                                    <ul>
                                        <li><a href="{{ url('cat') }}/{{$row['category']}}">{{$row['category']}}</a></li>
                                    </ul>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-buttom bg-black ptb-15">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="zm-copyright text-center">
                        <p class="uppercase"><img src="<?php echo asset('images/logo/logo.png')?>" style="width:20px;height:20px;"> &copy; 2019 KamiBijak</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>