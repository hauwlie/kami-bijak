<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sidebar-warp columns">
    <div class="row">                      
        <div class="col-md-12 col-sm-6 col-lg-12 sm-mt-60 xs-mt-60">
            <aside class="subscribe-form bg-gray text-center sidebar">
                <h3 class="uppercase zm-post-title">Subscribe to our
                    <br>
                Newsletter</h3>
                <form action="#" style="padding-top: 10px;">
                    <input placeholder="Enter email address" required="" type="email">
                    <input value="Subscribe" type="submit">
                </form>
                <p>Don't worry, we don't spam</p>
            </aside>
        </div>

        <div class="col-md-12 col-sm-6 col-lg-12 sm-mt-60 xs-mt-60" style="padding-top: 25px;">
            <h3>Follow us</h3>
            <aside class="text-center">
                <ul>
                    <li style="padding: 2px;"><a href="https://www.facebook.com/KamiBijakID"><i class="col-md-12 fa fa-facebook" style=" padding: 10px;background:#3b5998;"></i></a></li>
                    <li style="padding: 2px;"><a href="https://twitter.com/KamiBijakID"><i class="col-md-12 fa fa-twitter" style="padding: 10px;background:#1da1f2;"></i></a></li>
                    <li style="padding: 2px;"><a href="https://www.instagram.com/kamibijakid"><i class="col-md-12 fa fa-instagram" style="padding: 10px; background:#c32aa3;"></i></a></li>
                    <li style="padding: 2px;"><a href="https://www.youtube.com/channel/UClH-iALhZ2niASkp-XSNGpA"><i class="col-md-12 fa fa-youtube" style="padding: 10px; background: #db4437;"></i></a></li>
                </ul>
            </aside>
        </div>

        <div class="advertisement">
            <div class="row mt-40">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" >
                    <a href="http://merahputih.com/"><img src="<?php echo asset('images/ad/2.png')?>" alt=""></a>
                </div>
            </div>
        </div>       

    </div>
</div>
<div>
    
</div>