<!--Start sidenav -->
<nav id="mySidenav" class="sidenav navbar-inverse navbar-fixed-top align-self-center" id="sidebar-wrapper" role="navigation">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <ul class="nav sidebar-nav">       
        <p> </p>
        <li>
            <a href="{{ url('/') }}" class="text-grey">HOME</a>
        </li>
        <li>
            <a href="#" class="text-grey">Pilihan Editor</a>
        </li>
        <li>
            <a href="{{ url('Trending') }}" class="text-grey">Trending View</a>
        </li>
        <p style="color: grey;">______________________________ </p>
        <li>
            <a href="#" class="text-grey">History</a>
        </li>
        <li>
            <a href="#" class="text-grey">Tersimpan</a>
        </li>
        <p style="color: grey;">______________________________ </p>
        <li>
            <a href="#" class="text-grey">Setting</a>
        </li>
        <li>
            <a href="#" class="text-grey">Help</a>
        </li>
        <p style="color: grey;">______________________________ </p>
        <p style="color: grey;"><br> About Contat us Adversite Developer </p>
        <p style="color: grey;"><br> Terms Privasi Poicy & Safety  test </p>
        <p style="color: grey;">@2019 Kamibijak.com </p>
    </ul>
</nav>
<!-- End sidenav -->
    
<!-- <div class="navbar-kb black py-4">
    <div class="row m-0" >
        <div class="col-2 align-self-center">
            <div class="w-100 d-flex">
               <div class="w-50 " style="display: inline-grid;">   
                 <button type="button" class="float-left btn-nav" onclick="openNav()" >
                    <img src="<?php echo asset('images/menu.svg')?>" class="w-100 h-auto float-left" alt="menu" /> 
                    <a href=""><i class="fa fa-envelope-o" style="font-size: 30px;"></i></a>
                 </button>      
                </div>
                <div class="logo" >
                    <a href="{{ url('/') }}" class="text-grey" > <img src="<?php echo asset('images/logo.png')?>" class="w-100 h-auto float-left" align="logo" alt="main logo" /></a>
                </div>
            </div>

        </div>
        <div class="col-4 col-lg-3 align-self-center small-columns">
            <input class="form-control" type="text" placeholder="Cari disini">
        </div>
        <div class="col-4 col-lg-5 align-self-center">
            <ul class="list-unstyled">
                <li class="float-left"><a href="{{ url('cat') }}/{{'Infosiana'}}" class="text-grey">INFOSIANA</a></li>
                <li class="float-left"><a href="{{ url('cat') }}/{{'Hiburan'}}" class="text-grey">HIBURAN & GAYA HIDUP</a></li>
                <li class="float-left"><a href="{{ url('cat') }}/{{'Kuliner'}}" class="text-grey">KULINER</a></li>
                <li class="float-left"><a href="#" class="text-white btn-upload red ">Upload</a></li>
            </ul>
        </div>
        <div class="col-2 align-self-center">
            <div class="w-100 d-flex">
                <div class="notif-sec " style="display: inline-grid;width: 20%;">
                    <div class="message-notif position-relative">
                        <a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                            <span class="red rounded-circle position-absolute text-white align-bottom p-1" style="font-size: .65rem;right: 23%;left: auto;margin-top: -12px;">25</span>
                        <a href="#"><i class="fa fa-bell-o" aria-hidden="true"></i></a>
                            <span class="red rounded-circle position-absolute text-white align-bottom p-1" style="font-size: .65rem;right: 23%;left: auto;margin-top: -12px;">54</span>
                       
                    </div>
                </div>
                <div class="notif-sec " style="display: inline-grid;width: 60%;">
                    <div class="message-notif position-relative text-right">
                        <a href="{{ url('admin') }}">
                            <span class="text-grey" style="font-size: .75rem;">John Doe</span>
                            <img src="<?php echo asset('images/ava.jpg')?>" class="align-top rounded-circle ml-2" style="width: 25px;height: 25px;" alt="mess" />        
                        </a>                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>   -->




<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="collapse navbar-collapse">
        <div class="mr-auto">
            <a href="#" class="navbar-toggler-icon" onclick="openNav()"></a>
        </div>
        <div class="navbar-nav mr-auto mt-2 mt-lg-0">
            <a href="{{ url('/') }}"><img width="150" height="45" src="<?php echo asset('images/logo.png')?>" alt="main logo" /></a>
        </div>
    </div>
    

    <div class="collapse navbar-collapse">
        <form class="navbar-nav mr-auto mt-2 mt-lg-0" action="{{ url('query') }}" method="GET">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="q" >
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
        <ul class="navbar-nav mr-auto ">
            <li class="nav-item">
                <a class="nav-link" href="{{ url('cat') }}/{{'Infosiana'}}">INFOSIANA <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('cat') }}/{{'Hiburan'}}">HIBURAN & GAYA HIDUP</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('cat') }}/{{'Kuliner'}}">KULINER</a>
            </li>
            <!-- <li class="nav-item">
                <a class="nav-link text-white btn-upload red" href="{{ url('upload') }}">UPLOAD</a>
            </li> -->
        </ul>
    </div>

    <!-- <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#"><i class="fa fa-bell-o" aria-hidden="true"></i></a>
            </li>
        </ul>
        
        <div>
            <span class="login-btn nav-link text-white btn-upload blue"role="button">LOGIN
            <i class="fa fa-sign-in" aria-hidden="true"></i></span>
            <div class="login-form-wrap bg-white" style="display: none;">
                <form class="zm-signin-form text-left">
                <input type="text" class="zm-form-control username" placeholder="Username or Email">
                <input type="password" class="zm-form-control password" placeholder="Password">
                <input type="checkbox" value=" Remember Me" class="remember"> &nbsp;Remember Me<br>
                <div class="zm-submit-box clearfix  mt-20">
                    <input type="submit" value="Login">
                    <a href="{{ url('registration') }}">Register</a>
                </div>
                <a href="#" class="zm-forget">Forget username/password?</a>
                <div class="zm-login-social-box">
                    <a href="#" class="social-btn bg-facebook block"><span class="btn_text"><i class="fa fa-facebook"></i>Login with Facebook</span></a>
                    <a href="#" class="social-btn bg-twitter block"><span class="btn_text"><i class="fa fa-twitter"></i>Login with Twitter</span></a>
                </div>
                </form>
            </div>
        </div>
        
               

            <a href="{{ url('admin') }}">
                <span class="text-grey" style="font-size: .75rem;">John Doe</span>
                <img src="<?php echo asset('images/ava.jpg')?>" class="align-top rounded-circle ml-2" style="width: 25px;height: 25px;" alt="mess" />        
            </a>
   
    </div> -->
</nav>

<a id="scrollUp" href="#top" style="display: block; position: fixed; z-index: 2147483647;"><i class="fa fa-angle-up"></i></a>

