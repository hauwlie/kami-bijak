<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>KamiBijak - {{$postsdetail['title']}} </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo asset('images/logo/logo.png')?>">
        
        <!-- All css files are included here. -->
        <link rel="stylesheet" href="<?php echo asset('css/bootstrap.min.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/core.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/shortcode/shortcodes.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/style.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/responsive.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/custom.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/style-customizer.css')?>">
         <!-- Import Font -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <!-- Import Slick CSS -->
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <!-- Import Icon CSS-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

        <!-- Modernizr JS -->
        <script src="<?php echo asset('js/vendor/modernizr-2.8.3.min.js')?>"></script>
    </head>
    <body>
        <div class="wrapper">
            <header>
                @include('layout.header')            
            </header>

            <section id="page-content" class="page-wrapper">
                <div class="zm-section bg-white pt-30 xs-pt-30 sm-pt-30 pb-40">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 columns">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php
                                              // $view=$postsdetail['viewed']+1;
                                              // $title=$postsdetail['title']
                                              // mysqli_query("Update posts Set  title='$title', viewed='$view' Where id='$id'");
                                        ?>
                                        <article class="zm-post-lay-single">
                                            <div class="zm-post-thumb">
                                                    <iframe class="iframewh2" src="{{$postsdetail['embed']}}" frameborder="0" allow="encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            </div>
                                            <div class="zm-post-dis">
                                                <div class="zm-post-header">
                                                    <h2 class="zm-post-title h2">{{$postsdetail['title']}}</h2>
                                                    <p class="card-text">{{$postsdetail['viewed']}} Views</p>
                                                    <div class="zm-post-meta">
                                                         <ul>
                                                            <li class="s-meta"><a href="#" class="zm-author">{{$postsdetail['publisher']}}</a></li>
                                                            <li class="s-meta"><a href="#" class="zm-date">{{$postsdetail['created_at']->format('d M Y')}}</a></li>
                                                         </ul>
                                                    </div>
                                                </div>                                        
                                            </div>
                                            <div class="zm-post-content">
                                                {!! str_replace('\n',"<br />", $postsdetail->content) !!} 
                                            </div>
                                            
                                        </article>
                                    </div>
                                    <p>__________________________________________________________________________________________________________________________</p>
                                </div>   

                                <div class="row mb-40" style="padding-top: 25px">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="section-title">                       
                                            <h2 class="h6 header-color inline-block uppercase"><i class="fa fa-rss" aria-hidden="true"></i> Video Terbaru</h2>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="zm-posts">
                                             @foreach($postnew as $row)
                                            <article class="zm-post-lay-c zm-single-post clearfix">
                                                <div class="zm-post-thumb f-left">
                                                    <iframe class="iframewh" src="{{$row['embed']}}" frameborder="0" allow="encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                </div>
                                                <div class="zm-post-dis f-right">
                                                    <div class="zm-post-header">
                                                         <?php 
                                                        if ($row->category=="Infosiana") {
                                                             echo '<div class="zm-category" ><a href="cat/'.$row->category.'" class="bg-cat-1 cat-btn">'.$row->category.'</a></div>';
                                                         }elseif ($row->category=="Hiburan") {
                                                             echo '<div class="zm-category" ><a href="cat/'.$row->category.'" class="bg-cat-2 cat-btn">'.$row->category.'</a></div>';
                                                         }elseif ($row->category=="Kuliner") {
                                                             echo '<div class="zm-category" ><a href="cat/'.$row->category.'" class="bg-cat-3 cat-btn">'.$row->category.'</a></div>';
                                                         }

                                                        
                                                        ?>
                                                        <h2 class="zm-post-title"><a href="{{ url('detail') }}/{{$row['id']}}">{{$row['title']}}</a></h2>
                                                        <div class="zm-post-meta">
                                                            <ul>
                                                                <li class="s-meta"><a href="#" class="zm-author">{{$row['publisher']}}</a></li>
                                                                <li class="s-meta"><a href="#" class="zm-date">{{$row['created_at']->format('d M Y')}}</a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="zm-post-content">
                                                            <p>{!! $row['cont_sum'] !!}.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            @endforeach
                                            <button class="col-md-12 btn btn-primary">Load More</button> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @include('layout.MostView')
                        </div>  
                    </div>
                </div>
            </section>
            @include('layout.footer')
        </div>

        
        

        <script src="<?php echo asset('js/vendor/jquery-1.12.1.min.js')?>"></script>
        <script src="<?php echo asset('js/bootstrap.min.js')?>"></script>
        <script src="<?php echo asset('js/owl.carousel.min.js')?>"></script>
        <script src="<?php echo asset('js/plugins.js')?>"></script>
        <script src="<?php echo asset('js/main.js')?>"></script>
        <script>
            $(document).ready(function(){
                     var display_txt = display_txt.replace(/\n/g, "<br />");
                     $('#somediv').html(display_txt).css("color", "green");
            });
            
        </script>
    </body>
</html>
