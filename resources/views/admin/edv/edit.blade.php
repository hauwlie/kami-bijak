@extends('admin.master_edv')

@section('content')
	<div class="container-full">
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title">Data Tables</h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="{{ url('admin') }}"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page">Tables</li>
								<li class="breadcrumb-item" aria-current="page">Data Tables</li>
								<li class="breadcrumb-item active" aria-current="page">Edit Table</li>
							</ol>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<section class="content">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Edit Table</h3>
				</div>
				<div class="box-body">
					<form method="patch" action="{{action('AdminPostsController@update', $id)}}">
					   	{{csrf_field()}}



					   	<div class="form-group">
							<h3>Title</h3>
							<input type="text" name="title" class="form-control" value="{{$posts->title}}"/>
						</div>
						<div class="form-group">
							<h3>Category</h3>
							<input type="text" name="category" class="form-control" value="{{$posts->category}}"/>
						</div>
						<div class="form-group">
							<h3>Content</h3>
							<textarea id="compose-textarea" name="content" class="form-control" style="height: 150px">
								{{$posts->content}}
							</textarea>
						</div>
						<div class="form-group">
							<h3>Slug</h3>
							<input type="text" name="slug" class="form-control" value="{{$posts->slug}}"/>
						</div>
						<div class="form-group">
							<h3>Embed</h3>
							<input type="text" name="embed" class="form-control" value="{{$posts->embed}}"/>
						</div>
						<div class="form-group">
							<h3>Publisher</h3>
							<input type="text" name="publisher" class="form-control" value="{{$posts->publisher}}"/>
						</div>
						<div class="form-group">
							<h3>Viewed</h3>
							<input type="text" name="viewed" class="form-control" value="{{$posts->viewed}}"/>
						</div>
						<div class="form-group">
							<h3>Summary Content</h3>
							<textarea id="compose-textarea1" name="cont_sum" class="form-control" style="height: 150px">
								{{$posts->cont_sum}}
							</textarea>
						</div>
						<div class="form-group box-footer">
							<div class="pull-right">
								<input type="submit" class="btn btn-rounded btn-success" value="Edit">
								<a href="{{ url('datatable') }}" class="btn btn-rounded btn-danger">Discard</a>
							</div>
						</div>
					   	<!-- <div class="form-group">
					    	<input type="submit" class="btn btn-primary" value="Edit" />
					   	</div> -->
					</form>
				</div>
			</div>
		</section>
	</div>
@endsection