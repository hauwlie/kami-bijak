@extends('admin.master_edv')

@section('content')
	<div class="container-full">
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title">Data Tables</h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="{{ url('admin') }}"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page">Tables</li>
								<li class="breadcrumb-item" aria-current="page">Data Tables</li>
								<li class="breadcrumb-item active" aria-current="page">View Table</li>
							</ol>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<section class="content">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">View Data Id: {{$posts->id}}</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<h2>Title</h2>
								<h4 class="form-control-static">{{$posts->title}}</h4>
							</div>
							<div class="form-group">
								<h2>Category</h2>
								<h4 class="form-control-static">{{$posts->category}}</h4>
							</div>
							<div class="form-group">
								<h2>Content</h2>
								<h4 class="form-control-static">{{$posts->content}}</h4>
							</div>
							<div class="form-group">
								<h2>Slug</h2>
								<h4 class="form-control-static">{{$posts->slug}}</h4>
							</div>
							<div class="form-group">
								<h2>Embed</h2>
								<h4 class="form-control-static">{{$posts->embed}}</h4>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<h2>Publisher</h2>
								<h4 class="form-control-static">{{$posts->publisher}}</h4>
							</div>
							<div class="form-group">
								<h2>Viewed</h2>
								<h4 class="form-control-static">{{$posts->viewed}}</h4>
							</div>
							<div class="form-group">
								<h2>Created_at</h2>
								<h4 class="form-control-static">{{$posts->created_at}}</h4>
							</div>
							<div class="form-group">
								<h2>Update_at</h2>
								<h4 class="form-control-static">{{$posts->update_at}}</h4>
							</div>
							<div class="form-group">
								<h2>Summary Content</h2>
								<h4 class="form-control-static">{{$posts->cont_sum}}</h4>
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<a href="{{action('AdminPostsController@edit', $posts['id'])}}" class="btn btn-rounded btn-success"><i class="fa fa-envelope-o"></i> Edit</a>
						<a href="{{ url('datatable') }}" class="btn btn-rounded btn-danger"><i class="fa fa-times"></i> Back</a>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection