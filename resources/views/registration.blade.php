<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>KamiBijak </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- All css files are included here. -->
        <link rel="stylesheet" href="<?php echo asset('css/bootstrap.min.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/core.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/shortcode/shortcodes.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/style.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/responsive.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/custom.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/style-customizer.css')?>">
         <!-- Import Font -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <!-- Import Slick CSS -->
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <!-- Import Icon CSS-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

        <!-- Modernizr JS -->
        <script src="<?php echo asset('js/vendor/modernizr-2.8.3.min.js')?>"></script>
    </head>
    <body>
        <div class="wrapper">
            <header>
                @include('layout.header')            
            </header>
<div class="zm-section bg-white pt-40 pb-70 ">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-5">
                            <div class="section-title-2 mb-40">
                                <h3 class="inline-block uppercase">Cerate Your Account</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                    </div>
                    <div class="registation-form-wrap">
                        <form action="#">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <label>First Name</label> 
                                        <input placeholder="First Name" type="text">
                                    </div>
                                    <div class="single-input">
                                        <label>Email</label> 
                                        <input placeholder="Email" type="email">
                                    </div>
                                    <div class="single-input">
                                        <label>Address</label> 
                                        <input placeholder="Address" type="text">
                                    </div>
                                    <div class="single-input">
                                        <label>City</label> 
                                        <input placeholder="City" type="text">
                                    </div>
                                    <div class="single-input">
                                        <label>Password</label> 
                                        <input placeholder="Password" type="password">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <label>Last Name</label> 
                                        <input placeholder="Last Name" type="text">
                                    </div>
                                    <div class="single-input">
                                        <label>Phone Number</label> 
                                        <input placeholder="Phone Number" type="text">
                                    </div>
                                    <div class="single-input">
                                        <label>Postal Code</label> 
                                        <input placeholder="Postal Code" type="text">
                                    </div>
                                    <div class="single-input">
                                        <label>Country</label> 
                                        <input placeholder="Country" type="text">
                                    </div>
                                    <div class="single-input">
                                        <label>Confirm Password</label> 
                                        <input placeholder="Confirm Password" type="password">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                                    <div class="single-input">
                                        <input value="Remember" class="remember" type="checkbox">Saya Setuju <a href="#">Pelajari Lebih lanjut</a>
                                        <button class="submit-button mt-20 inline-block" type="submit">Register</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
                 @include('layout.footer')
        </div>

        <script src="<?php echo asset('js/vendor/jquery-1.12.1.min.js')?>"></script>
        <script src="<?php echo asset('js/bootstrap.min.js')?>"></script>
        <script src="<?php echo asset('js/owl.carousel.min.js')?>"></script>
        <script src="<?php echo asset('js/plugins.js')?>"></script>
        <script src="<?php echo asset('js/main.js')?>"></script>
    </body>
</html>