<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>KamiBijak </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- All css files are included here. -->
        <link rel="stylesheet" href="<?php echo asset('css/bootstrap.min.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/core.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/shortcode/shortcodes.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/style.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/responsive.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/custom.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/style-customizer.css')?>">

        <link rel="stylesheet" href="<?php echo asset('css/nyoba.css')?>">
        <link rel="stylesheet" href="<?php echo asset('css/admin/table/bootstrap3-wysihtml5.min.css')?>">


         <!-- Import Font -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <!-- Import Slick CSS -->
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <!-- Import Icon CSS-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

        <!-- Modernizr JS -->
        <script src="<?php echo asset('js/vendor/modernizr-2.8.3.min.js')?>"></script>
    </head>
    <body>
        <div class="wrapper">
            <header>
                @include('layout.header')         
            </header>
            
            <!-- Start page content -->
            <section class="content">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Create New Data</h3>
                    </div>
                    <div class="box-body">
                        <form method="post" action="{{url('datatable')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <h3>Title</h3>
                                <input type="text" name="title" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <h3>Category</h3>
                                <input type="text" name="category" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <h3>Content</h3>
                                <textarea id="compose-textarea" name="content" class="form-control" style="height: 150px">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <h3>Slug</h3>
                                <input type="text" name="slug" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <h3>Embed</h3>
                                <input type="text" name="embed" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <h3>Publisher</h3>
                                <input type="text" name="publisher" class="form-control"/>
                            </div>
                            <!-- <div class="form-group">
                                <h3>Viewed</h3>
                                <input type="text" name="viewed" value="0" class="form-control"/>
                            </div> -->
                            <div class="form-group">
                                <h3>Summary Content</h3>
                                <textarea id="compose-textarea1" name="cont_sum" class="form-control" style="height: 150px">
                                </textarea>
                            </div>
                            <div class="box-footer">
                                <div class="pull-right">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-rounded btn-success"><i class="fa fa-envelope-o"></i> Submit</button>
                                    </div>
                                </div>
                                <a href="{{ url('/') }}" class="btn btn-rounded btn-danger"><i class="fa fa-times"></i> Discard</a>
                            </div>
                            <!-- <div class="form-group">
                                <input type="submit" class="btn btn-rounded btn-success" />
                            </div> -->
                        </form>
                    </div>
                    
                </div>
            </section>
            <!-- End page content -->
            @include('layout.footer')
        </div>
        <script src="<?php echo asset('js/admin/jquery-3.3.1.js')?>"></script>
        <script src="<?php echo asset('js/admin/table/bootstrap.min.js')?>"></script>
        <script src="<?php echo asset('js/admin/table/bootstrap3-wysihtml5.all.min.js')?>"></script>
        <script src="<?php echo asset('js/admin/table/form-compose.js')?>"></script>
    </body>
</html>