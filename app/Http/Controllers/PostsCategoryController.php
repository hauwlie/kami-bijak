<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;

class PostsCategoryController extends Controller
{
    public function index()
    {
        
    }

    public function detail($id)
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($category)
    {
       $posts = Posts::where('category',$category)->orderBy('created_at', 'DESC')->paginate(10);
       $postsre = Posts::distinct()->get('category'); 
        return view('category', compact('posts','category'), compact('postsre'));;
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
