<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;

class PostsController extends Controller
{
    public function index()
    {
        // $posts = Posts::all()->toArray();
        $posts = Posts::orderBy('created_at', 'DESC')->limit(10)->get();
        $postsre = Posts::distinct()->get('category');
        return view('home', compact('posts'), compact('postsre'));;
    }

    public function regis(){
         $postsre = Posts::distinct()->get('category');
        return view('registration', compact('postsre'));;
    }

    public function show($id)
    {
       
    }
     public function create(Request $request)
    {
        $query = $request->get('q');
        $hasil = Posts::where('title', 'LIKE', '%' . $query . '%')->orderBy('created_at', 'DESC')->paginate(10);
        $postsre = Posts::distinct()->get('category');
        $posts = Posts::orderBy('created_at', 'DESC')->limit(10)->get(); 

        return view('result', compact('hasil', 'query'), compact('postsre'))->with('posts',$posts);
    }

}
