<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;

class AdminPostsController extends Controller
{
    public function index()
    {
        
        $posts = Posts::orderBy('id', 'DESC')->paginate(5);
        return view('admin.edv.dashboard_data', compact('posts'));
    }
    public function create()
    {
        return view('admin.edv.create');
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required',
            'category'=>'required',
            'content'=>'required',
            'slug'=>'required',
            'embed'=>'required',
            'publisher'=>'required',
            'viewed'=>'required',
            'cont_sum'=>'required'
        ]);
        $posts = new Posts([
            'title'    =>  $request->get('title'),
            'category'     =>  $request->get('category'),
            'content'     =>  $request->get('content'),
            'slug'     =>  $request->get('slug'),
            'embed'     =>  $request->get('embed'),
            'publisher'     =>  $request->get('publisher'),
            'viewed'     =>  $request->get('viewed'),
            'cont_sum'     =>  $request->get('cont_sum')
        ]);
        $posts->save();
        return redirect()->route('datatable.index');
    }
    public function show($id)
    {
        $posts = Posts::find($id);
        return view('admin.edv.view', compact('posts', 'id'));
    }
    public function edit($id)
    {
        $posts = Posts::find($id);
        return view('admin.edv.edit', compact('posts', 'id'));
    }
    public function update(Request $request, $id)
    {
         $this->validate($request,[
            'title'=>'required',
            'category'=>'required',
            'content'=>'required',
            'slug'=>'required',
            'embed'=>'required',
            'publisher'=>'required',
            'viewed'=>'required',
            'cont_sum'=>'required'
        ]);
        $posts = Posts::find($id);
        $posts->title = $request->get('title');
        $posts->category = $request->get('category');
        $posts->content = $request->get('content');
        $posts->slug = $request->get('slug');
        $posts->embed = $request->get('embed');
        $posts->publisher = $request->get('publisher');
        $posts->viewed = $request->get('viewed');
        $posts->cont_sum = $request->get('cont_sum');
        $posts->save();
        return redirect()->route('datatable.index');
    }
    public function destroy($id)
    {
        $posts = Posts::findOrFail($id);
        $posts->delete();
        return redirect()->route('datatable.index');
    }
}
