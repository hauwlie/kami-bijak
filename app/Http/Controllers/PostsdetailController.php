<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;

class PostsdetailController extends Controller
{
    
    public function index()
    {
        //
    }

    public function detail($id)
    {
        $postsdetail = Posts::find($id);
        $postsmost= Posts::orderBy('viewed','DESC')->orderBy('created_at','DESC')->limit(4)->get();
        $postsre = Posts::distinct()->get('category');
        $postnew =Posts::orderBy('created_at','DESC')->limit(6)->get();
        return view('detail', compact('postsdetail', 'id'), compact('postsmost'))->with('postnew',$postnew)->with('postsre',$postsre);
    }

   
    public function create()
    {
        //
    }

   
    public function store(Request $request)
    {
        //
    }

    
    public function show( )
    {
        $postsTrending= Posts::orderBy('viewed','DESC')->orderBy('created_at','DESC')->paginate(10);
         $postsre = Posts::distinct()->get('category');
        return view('trending', compact('postsTrending'), compact('postsre'));
    }

    
    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
