<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;

class UploadPostsController extends Controller
{
    public function index()
    {
    	$postsre = Posts::distinct()->get('category');  
        return view('upload', compact('postsre'));
    }
}
