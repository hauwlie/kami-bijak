<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Route::resource('/', 'PostsController');
Route::post('home','PostsController@postindex');
Route::get('detail/{id}','PostsdetailController@detail');
Route::get('Trending','PostsdetailController@show');
Route::resource('upload', 'UploadPostsController');



// admin panel
Route::get('admin', function () { return view('admin.dashboard'); });
Route::resource('datatable', 'AdminPostsController');
Route::get('delete/{id}', 'AdminPostsController@destroy');
Route::get('update/{id}', 'AdminPostsController@update');
// category panel
Route::get('cat/{category}', 'PostsCategoryController@show');
Route::get('query', 'PostsController@create');

Route::get('registration', 'PostsController@regis');
// Route::get('registration', function () { 
// 	$postsre = Posts::distinct()->get('category');  
// 	return view('registration', compact('postsre')); 
// });